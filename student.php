<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .display{
            margin-left: 30px;
        }
        .selectbox{
            margin-left: 30px;
            padding-right: 42px;
        }
        .button{
            border-radius: 5px;
            border: 2px solid rgb(5, 5, 82);
            padding: 10px 30px;
            background-color: blue;
            color: #fff;
            margin-left: 110px;
            margin-top: 20px;
        }
        .Add{
            height: 30px;
            padding: 0px 25px;
            margin-top: 15px;
            margin-left: 460px;
            background-color: blue;
            color: #fff;
            border: 2px solid rgb(99, 99, 232);
            
        }
        .custom{
            margin-left: 300px;
        }
        .click{
            padding: 5px 10px;
            background-color: rgb(114, 114, 249);
            color: #fff;
            border: 2px solid blue;
            
        }
    </style>
</head>
<body>
     
    <fieldset style="width:60%">
    <form action='' method='POST'>
        <div style="margin-left:20% ;">
            <table >
                <tr>
                    <td >Khoa</td>
                    <td><select class='selectbox' name = 'Khoa' value = ''>
                        <?php
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                            echo "
                            <option ";
                            echo isset($_POST['Khoa']) && $_POST['Khoa'] == $key ? "selected " : "";
                            echo "  value='" . $key . "'>" . $value  . "</option>";
                            }
                            
                        ?>
                        </select></td>
                    
                </tr>
                <tr>
                    <td >Từ khóa</td>
                    <td><input  class="display" type="text" name="" id=""></td>
                </tr>
                
            </table>
            <button class="button">tìm kiếm</button>
            
        </div>

        <div >
            <div style="display:flex ;">
                <p>Số sinh viên tìm thấy:xxx</p>
                <?php
                    if (isset($_POST["signup"]))
                    { 
                        header("Location: ../day07/signup.php");
                    }
                ?>
                <input class="Add" type="submit" name='signup'  value="Thêm"></input>
            </div>
            <table >
                <tr>
                    <td >
                        <p>No</p>
                    </td>
                    <td>
                        <p class="display">Tên sinh viên</p>
                    </td>
                    <td>
                        <p class="display">Khoa</p>
                    </td>
                    <td ><p class="custom">Action</p></td>
                </tr>

                <tr>
                    <td >
                        <p>1</p>
                    </td>
                    <td>
                        <p class="display">Nguyễn Văn A</p>
                    </td>
                    <td>
                        <p class="display">Khoa học máy tính</p>
                    </td>
                    <td>
                        <button class="custom click">Xóa</button>
                        <button class="click">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td >
                        <p>2</p>
                    </td>
                    <td>
                        <p class="display">Trần Thị B</p>
                    </td>
                    <td>
                        <p class="display">Khoa học máy tính</p>
                    </td>

                    <td>
                        <button class="custom click">Xóa</button>
                        <button class="click">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td >
                        <p>3</p>
                    </td>
                    <td>
                        <p class="display">Nguyễn Hoàng C</p>
                    </td>
                    <td>
                        <p class="display">Khoa học vật liệu</p>
                    </td>

                    <td>
                        <button class="custom click">Xóa</button>
                        <button class="click">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td >
                        <p>4</p>
                    </td>
                    <td>
                        <p class="display">Đinh Quang D</p>
                    </td>
                    <td>
                        <p class="display">Khoa học vật liệu</p>
                    </td>

                    <td>
                        <button class="custom click">Xóa</button>
                        <button class="click">Sửa</button>
                    </td>
                </tr>

                
            </table>
           
            
            
        </div>

    </fieldset>
   
    
</body>
</html>